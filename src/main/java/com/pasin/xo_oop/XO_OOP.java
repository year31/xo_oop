/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.xo_oop;

/**
 *
 * @author Pla
 */
public class XO_OOP {

    public static void main(String[] args) {
        Game game = new Game();
        game.showWelcome();
        game.newBoard();
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()){
                game.showTable();
                game.showResult();
                game.showStat();
                game.newBoard();
            }

        }

    }
}
